Set-ExecutionPolicy -Scope Process -ExecutionPolicy Bypass
# Self-elevate the script if required
if (-Not ([Security.Principal.WindowsPrincipal] [Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] 'Administrator')) {
  if ([int](Get-CimInstance -Class Win32_OperatingSystem | Select-Object -ExpandProperty BuildNumber) -ge 6000) {
   $CommandLine = "-File `"" + $MyInvocation.MyCommand.Path + "`" " + $MyInvocation.UnboundArguments
   Start-Process -FilePath PowerShell.exe -Verb Runas -ArgumentList $CommandLine
   Exit
  }
 }

 Clear-Host
 Write-Host -ForegroundColor Red -BackgroundColor Yellow "Cisco VPN Wizard by Cedric (v1.0)"
 Start-Sleep -Seconds 1
 Write-Host "Checking if Cisco VPN is already installed..."
 Start-Sleep -Seconds 1

 #Defines the target destination for files
 [string]$Destination = "C:\Program Files (x86)\Cisco\Cisco AnyConnect Secure Mobility Client"

 if ( (Test-Path "$Destination\vpncli.exe" -PathType leaf) ){
  Write-Host "Cisco Client installation found"
  Start-Sleep -Seconds 1
  Write-Host "Disabling the autostart function"
}
else{
 Write-Host "Could not find the Installation Path of Cisco Client" 
 Start-Sleep -Seconds 1
 Write-Host "Trying to download it..."
 Write-Host "Please wait..."
 

#Name of the File
$Installer = 'anyconnect-win-4.8.02045-core-vpn-predeploy-k9.msi'

#Source URL for download
$url = "https://cr.rwth-aachen.de/uploads/anyconnect-win-4.8.02045-core-vpn-predeploy-k9.msi"
$output = "$PSScriptRoot\$Installer"
$start_time = Get-Date

$client = (New-Object System.Net.WebClient)
$client.Credentials = (New-Object System.Net.NetworkCredential("donald_duck", "Cuziyim%13"))
$client.DownloadFile($url, $output)
Write-Host "Download complete!"

Write-Output "Time taken: $((Get-Date).Subtract($start_time).Seconds) second(s)"
Start-Sleep -Seconds 1

#Installer
Write-Host "Starting the Installation..."

# standalone-installer: Cisco AnyConnect
#& "$output" /quiet /passive /norestart
Install-Package $output

if (-Not (Test-Path "$Destination\vpncli.exe" -PathType leaf)) {
	Write-Host -ForegroundColor Red "Installation failed, aborting..."
	Write-Host -NoNewLine 'Press any key...';
	$null = $Host.UI.RawUI.ReadKey('NoEcho,IncludeKeyDown');
	Exit 2
}

Write-Host "Cisco Client Installed!"
Remove-Item -Path "$PSScriptRoot\$Installer"

}


#Removing it from Autostart
Set-ItemProperty -Path HKLM:\Software\Microsoft\Windows\CurrentVersion\Explorer\StartupApproved\Run32 -Name 'Cisco AnyConnect Secure Mobility Agent for Windows' -Value ([byte[]](0x33,0x32,0xFF))
Start-Sleep -Seconds 1
Write-Host "Cisco Autostart disabled"
Start-Sleep -Seconds 1

  


#Ask for Username and password
Write-Host "Please insert TIM-Username and Password, you can copy paste if yoo like"
$us = Read-Host 'TIM-Username' 
$pw = Read-Host 'TIM-Password' 

#Sets up the connect.txt
function CreateSettings([bool]$split_tunnel = $true)
{
  if ($split_tunnel) {
      $filename = "$Destination\connect.txt"
  } else {
      $filename = "$Destination\connect2.txt"
  }
  
  New-Item "$filename" | Out-Null
  Add-Content "$filename" "connect vpn.rwth-aachen.de (IPsec)"
  
  if ($split_tunnel) {
      Add-Content "$filename" "RWTH-VPN (Split Tunnel)"
  } else {
      Add-Content "$filename" "RWTH-VPN (Full Tunnel)"
  }
  
  Add-Content "$filename" "$us"
  Add-Content "$filename" "$pw"
}

#Save Userinput as txt file at given location
if (Test-Path "$Destination\connect.txt" -PathType leaf)
{
  Remove-Item "$Destination\connect.txt" 
}
CreateSettings

if (Test-Path "$Destination\connect2.txt" -PathType leaf)
{
  Remove-Item "$Destination\connect2.txt" 
}
CreateSettings $false

#Creatig other needed Files

$ConnectBat = @"
taskkill /IM vpnui.exe /T /F  
vpncli.exe -s < %~n0.txt
"@

$DisconnectBat = @"
vpncli.exe -s < %~n0.txt
"@

$DisconnectTxt = @"
disconnect vpn.rwth-aachen.de (IPsec)
"@

#Force new Files (Overwrites old if it exists)
New-Item $Destination\connect.bat -Value "$ConnectBat" -Force | Out-Null
New-Item $Destination\connect2.bat -Value "$ConnectBat" -Force | Out-Null
New-Item $Destination\disconnect.bat -Value "$DisconnectBat" -Force | Out-Null
New-Item $Destination\disconnect.txt -Value "$DisconnectTxt" -Force | Out-Null

#Create Shortcuts
$WShell = New-Object -ComObject WScript.Shell

$DesktopPath = [System.Environment]::GetFolderPath([System.Environment+SpecialFolder]::Desktop)

#Connect
$Shortcut = $WShell.CreateShortcut("$DesktopPath\Connect to VPN (RWTH).lnk")
$shortcut.WorkingDirectory = $Destination
$Shortcut.TargetPath = $Destination+'\connect.bat'
$Shortcut.Save()
#Connect2
$Shortcut = $WShell.CreateShortcut("$DesktopPath\Connect to VPN --Full Tunnel (RWTH).lnk")
$shortcut.WorkingDirectory = $Destination
$Shortcut.TargetPath = $Destination+'\connect2.bat'
$Shortcut.Save()
#Disconnect
$Shortcut = $WShell.CreateShortcut("$DesktopPath\Disconnect from VPN (RWTH).lnk")
$shortcut.WorkingDirectory = $Destination
$Shortcut.TargetPath = $Destination+'\disconnect.bat'
$Shortcut.Save()

Write-Host ""
Write-Host -ForegroundColor Green 'Script finished and successfull';
Write-Host -ForegroundColor Green 'Check the three shortcuts created on your desktop!';
Write-Host -NoNewLine 'Press any key to continue...';
$null = $Host.UI.RawUI.ReadKey('NoEcho,IncludeKeyDown');
